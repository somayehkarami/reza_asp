﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace BuiltInHelperMethodes.Models
{
    public class Person
    {

        //[HiddenInput]
        [HiddenInput(DisplayValue = false)]
        public int PersonId { get; set; }


        [Display(Name = "Name")]
        public string FirstName { get; set; }


        [Display(Name= "Family") ]
        public string LastName { get; set; }


        public DateTime Birthday { get; set; }

        public Address HomeAddress { get; set; }
        
        public bool IsApproved { get; set; }

        public Role Role { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Filters.InfraStrucure;

namespace Filters.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [CustomAuth(false)]
        public  String Index()
        {
            return "This is the Index action on the home controller";
        }
        [MySiteAuth]
        public String List()
        {
            return "This is the list action on the home Controller";
        }
        [RangeException]
        public string RangeTest(int id)
        {
            if (id > 100)
            {
                return String.Format("The id value is: {0}", id);
            }
            else
            {
                throw new ArgumentOutOfRangeException("id", id, "");
            }
        }
    }
}
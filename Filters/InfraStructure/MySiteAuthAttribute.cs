﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace Filters.InfraStrucure
{
    public class MySiteAuthAttribute:FilterAttribute,IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            IIdentity  ident= filterContext.Principal.Identity;//To find identity of user
            if (!ident.IsAuthenticated || !ident.Name.EndsWith("@mysite.com"))
            {
                filterContext.Result = new HttpUnauthorizedResult();//if the user authorized or not
            }
        }


       

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    {"controller", "MySiteAccount"},
                    {"action", "Login"},
                    {"returnUrl", filterContext.HttpContext.Request.RawUrl}
                });
            }
        }

       
    }
}
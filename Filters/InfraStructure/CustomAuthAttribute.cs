﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.InfraStrucure
{
    public class CustomAuthAttribute :AuthorizeAttribute
    {
        private bool loaclAllowed;

        public CustomAuthAttribute(bool allowedParam)
        {
            loaclAllowed = allowedParam;

        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.IsLocal)//detect the request is local request or not
            {
                return loaclAllowed;
            }
            else
            {
                return true;
            }

        }

    }
}
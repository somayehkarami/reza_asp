﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LibraryStudents.Data.Interfaces;
using LibraryStudents.Data.Models;
using StudentProject.Models;
using LibraryStudents.Data.Repositories;

namespace StudentProject.Controllers
{
    public class StudentsController : ApiController
    {

        /*******************************************Dependency Injection*******************************/

        private IStudentRepository students = new StudentRepository();
        
        public StudentsController(IStudentRepository _student)
        {
            this.students = _student;
        }


        /*******************************************Get All Students**************************************/
        // GET api/students
        public IEnumerable<Student> Get()
        {
            return students.GetAllStudents();
        }




        /******************************************Get Student By ID***************************************/
        // GET api/students/2
        public IHttpActionResult Get(int id)
        {
            var student = students.GetStudentById(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }



        /***********************************ADD New Student*********************************************************/
        //model binding
        //this is not reguler this is web servie 
        [HttpPost]
        public IHttpActionResult PostStudent(Student student)
        {
           bool result= students.AddNewStudent(student);
           if (result)
           {
               return Ok(students);
           }

           return BadRequest();
        }




        /*************************************Delete Student***************************************************/
        [HttpDelete]
        public IHttpActionResult DeleteStudent(int id)
        {
            if (students.Remove(id))
            {
                return Ok(students.GetAllStudents());

            }

            return NotFound();

        }


        /***************************************Update Student***********************************************/
        [HttpPut]
        public IHttpActionResult UpdateStudent(int id, Student student)
        {
            var stud = students.UpdateStudent(id, student);
            if(stud != null)
            {
                return Ok(stud);
            }

            return NotFound();
        }
    }
}
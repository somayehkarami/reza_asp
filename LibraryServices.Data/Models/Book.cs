﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryServices.Data.Models
{
    public class Book//consider this class as DB table
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int PublicationYear { get; set; }
        public bool IsAvailable { get; set; }
        public string CallNumber { get; set; }



        //[foreignKey("Cost)]
        //public int? CostId { get; set; }

        //Navigation property
        //public virtual  Cost Cost { get; set; }

    }
}
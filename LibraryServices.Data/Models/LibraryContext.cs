﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryServices.Data.Models
{
    public  class LibraryContext:DbContext
    {
        //constructor
        public LibraryContext() : base("name=LibraryContext")// LibraryContext" parameter for base 
        {

        }
        //this line says we have this book in the models LibraryServices.Data 
        //create table of this model and name it books
        public DbSet<LibraryServices.Data.Models.Book>Books { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryServices.Data.Interfaces;
using LibraryServices.Data.Models;

namespace LibraryServices.Data.Repositories
{
    //here we just represent our data we dont call any Http Method
    public class BookRepository : IBookRepository
    {
        //since the book as table each record of the table be an object
        public List<Book> books = new List<Book>()
        {
            new Book()
            {
                Id = 1, Title = "The Girl on Train", Author = "Hawkins,Paula", PublicationYear = 2015,
                CallNumber = "F HAWKI"
            },
            new Book()
            {
                Id = 2, Title = "Rouge Lawyer", Author = "Grisham,John", PublicationYear = 2015, CallNumber = "F GRISH"
            }
        };

        public List<Book> GetAllBooks()
        {
            return books;
        }

        public Book GetBook(int id)
        {
            var book = books.FirstOrDefault(x => x.Id == id);//using method FirstOrDefault to find particle id
            return book;
        }

        public bool AddNewBook(Book book)
        {
            books.Add(book);
            return true;
        }

        public bool Remove(int id)
        {
            var book = GetBook(id);
            if (book == null)
            {
                return false;
            }

            books.Remove(book);
            return true;
        }

        public List<Book> UpdateBook(int id, Book book)
        {
            if (this.Remove(id))
            {
                this.AddNewBook(book);
                return books;
            }

            return books;
        }
    }
}

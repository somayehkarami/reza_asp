﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ModelsAndBinding.Infrastructure;

namespace ModelsAndBinding.Models
{
    public class Appointment
    {
        [NoJohnOnMondaysAttribute]

        [Required]//enter the name is require
        public string ClientName { get; set; }


        [FutureDate(ErrorMessage = "Please enter a date in the future")]
        public DateTime Date { get; set; }



        [MustBeTrue(ErrorMessage = "You must accept the terms")]
        public bool TermsAccepted { get; set; }
    }
}
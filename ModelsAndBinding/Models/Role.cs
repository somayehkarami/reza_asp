﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ModelsAndBinding.Models
{
    public enum Role
    {
        Admin,
        User,
        Guest
    }
}
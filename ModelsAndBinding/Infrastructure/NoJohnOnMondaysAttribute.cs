﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ModelsAndBinding.Models;

namespace ModelsAndBinding.Infrastructure
{
    public class NoJohnOnMondaysAttribute:ValidationAttribute
    {

        public NoJohnOnMondaysAttribute()
        {
            ErrorMessage = "John cannot book appointment on Monday";
        }

        public override bool IsValid(object value)
        {
            Appointment app = value as Appointment;

            if (app == null || string.IsNullOrEmpty(app.ClientName) || app.Date == null)
            {
                return true;
            }
            else
            {
                return !(app.ClientName.ToLower() == "john" && app.Date.DayOfWeek == DayOfWeek.Monday);
            }
        }
    }
}
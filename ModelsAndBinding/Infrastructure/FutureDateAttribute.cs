﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ModelsAndBinding.Infrastructure
{
    public class FutureDateAttribute:RequiredAttribute
    {
        //steps for checking
        //1-require for filling the field and the time
        public override bool IsValid(object value)
        {
            return base.IsValid(value) && ((DateTime) value) > DateTime.Now;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ModelsAndBinding.Infrastructure
{
    public class MustBeTrueAttribute:ValidationAttribute//inherit
    {
        //using syntax for validation of forms
        public override bool IsValid(object value)
        {
            return value is bool && (bool) value;
        }

    }
}
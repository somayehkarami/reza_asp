﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelsAndBinding.Models;

namespace ModelsAndBinding.Controllers
{
    public class BookingController : Controller
    {
        // GET: Booking
        public ActionResult  MakeBooking()
        {
            return View(new Appointment{Date = new DateTime(2017,1,1,0,0,0)});
        }

        [HttpPost]
        public ViewResult MakeBooking(Appointment appt)
        {
            if (ModelState.IsValid)//if every validation is true
            {
                //save to data  base 
                return View("Completed", appt);
            }
            else
            {
                return View();
            }
        }
    }
}
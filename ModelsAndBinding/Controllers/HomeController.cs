﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelsAndBinding.Models;

namespace ModelsAndBinding.Controllers
{
    public class HomeController : Controller
    {
        private Person[] personData =
        {
            new Person {PersonId = 1, FirstName = "John", LastName = "Bolton", Role = Role.Admin},
            new Person {PersonId = 2, FirstName = "Jack", LastName = "Rabbit", Role = Role.User},
            new Person {PersonId = 3, FirstName = "Mary", LastName = "Christams", Role = Role.User},
            new Person {PersonId = 4, FirstName = "Henry", LastName = "Tiery", Role = Role.Guest}
        };

        // GET: Home
        public ActionResult Index(int id=4)
        {
            Person dataItem = personData.Where(p => p.PersonId == id).First();
            return View(dataItem);
        }

        public ActionResult CreatePerson()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult CreatePerson(Person model)
        {
            return View("Index", model);
        }

        public ActionResult Names(string[] names)
        {
            names = names ?? new string[0];//check if it is null  add null string to the names
            return View(names);
        }
    }
}

/* Search Order:
 Request.Form["id"]
RoutData.Values["id"]
Request.QueryString["id"]
Request.File["id"]
*/
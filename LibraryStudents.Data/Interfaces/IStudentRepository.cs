﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryStudents.Data.Models;

namespace LibraryStudents.Data.Interfaces
{
   public interface IStudentRepository
   {
       List<Student> GetAllStudents();

       Student GetStudentById(int id);

       bool AddNewStudent(Student student);
       bool Remove(int id);
       List<Student> UpdateStudent(int id, Student student);


   }
}

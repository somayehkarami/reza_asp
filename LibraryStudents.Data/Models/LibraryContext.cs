﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryStudents.Data.Models
{
    public class LibraryContext:DbContext
    {
        public LibraryContext() : base("name=LibraryContext")
        {

        }
        // it have Student inside  models in LibraryStudents and create table name it  students
        public DbSet<LibraryStudents.Data.Models.Student> Students { get; set; }
    }
}

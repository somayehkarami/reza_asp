﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryStudents.Data.Interfaces;
using LibraryStudents.Data.Models;

namespace LibraryStudents.Data.Repositories
{
    public class StudentRepository : IStudentRepository
    {

        public List<Student> students = new List<Student>()
        {
            new Student()
            {
                Id = 1, FirstName = "Elsa", LastName = "Hossieni", NumberOfCourses = 2,
                GraduationDate = new DateTime(2012, 12, 31, 16, 45, 0)

            },
            new Student()
            {
                Id = 2, FirstName = "Sorin", LastName = "Hossieni", NumberOfCourses = 8,
                GraduationDate = new DateTime(2014, 10, 31, 16, 02, 0)

            }

        };
        

        public List<Student> GetAllStudents()
        {
            return students;
        }

        public Student GetStudentById(int id)
        {
            var student = students.FirstOrDefault(x => x.Id == id);
            return student;
        }
        
        public bool AddNewStudent(Student student)
        {
            students.Add(student);
            return true;
        }

        public bool Remove(int id)
        {
            var st = GetStudentById(id);
            if (st == null)
            {
                return false;
            }

            students.Remove(st);
            return true;
        }
        
        public List<Student> UpdateStudent(int id, Student student)
        {
            if (this.Remove(id))
            {
                this.AddNewStudent(student);
                return students;
            }

            return students;
        }
    }
}

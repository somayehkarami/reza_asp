﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryStudents.Data.Interfaces;
using LibraryStudents.Data.Models;

namespace LibraryStudents.Data.Repositories
{
    public class StudentDatabase : IStudentRepository
    {
        //connect to database in this class 
        private LibraryContext db = new LibraryContext();

        public bool AddNewStudent(Student student)
        {
            db.Students.Add(student);
            db.SaveChanges();
            return true;
        }

        public List<Student> GetAllStudents()
        {
            return db.Students.ToList();
        }

        public Student GetStudentById(int id)
        {
            return db.Students.FirstOrDefault(x => x.Id == id);
        }

        public bool Remove(int id)
        {
            var student = GetStudentById(id);
            if (student == null)
            {
                return false;
            }

            db.Students.Remove(student);
            db.SaveChanges();
            return true;
        }

        public List<Student> UpdateStudent(int id, Student student)
        {
            if (this.Remove(id))
            {
                this.AddNewStudent(student);
                db.SaveChanges();
                return db.Students.ToList();
            }

            return db.Students.ToList();
        }
    }
}

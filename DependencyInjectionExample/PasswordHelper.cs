﻿using System;
using System.Collections.Generic;
using System.Text;


namespace DependencyInjectionExample
{
    public class PasswordHelper 
    {
        private IEmailSender email_sender;
        

        public PasswordHelper(IEmailSender sender)//constructor injection
        {
            this.email_sender = sender;
        }


        public void setEmailSender(IEmailSender sender)//setter injection
        {
            this.email_sender = sender;
        }
        public void ResetPassword()
    
           {
               
              // EmailSender sender = new EmailSender();
              email_sender.Send();
    
             // sender.Send();
    
           }
    }
}

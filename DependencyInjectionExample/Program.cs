﻿using System;

namespace DependencyInjectionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            PasswordHelper passs_helper = new PasswordHelper(new EmailSender());

            //
            passs_helper.setEmailSender(new FakeEmailSender());
            passs_helper.setEmailSender(new EmailSender());
            PasswordHelper passs_helper2 = new PasswordHelper(new FakeEmailSender());//test
            
        }
    }

}
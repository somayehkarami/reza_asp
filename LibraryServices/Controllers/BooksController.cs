﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LibraryServices.Data.Interfaces;
using LibraryServices.Data.Models;
using LibraryServices.Data.Repositories;
using LibraryServices.Models;
using Microsoft.Ajax.Utilities;

namespace LibraryServices.Controllers
{
    public class BooksController : ApiController//like valuescontrollers
    {
        //since the book as table each record of the table be an object

        //private IBookRepository books= new BookRepository();// ==>if i use this line i do not need from line 19 to 24


        /*******************************************Dependency Injection*******************************/
        private IBookRepository books;
        public BooksController(IBookRepository _book)
        {
            this.books = _book;
        }

        /*******************************************Get All Books**************************************/
        [HttpGet]
        // GET api/books
        public IEnumerable<Book> Get()
        {
            return books.GetAllBooks();
        }


        /******************************************Get Book By ID***************************************/
        [HttpGet]
        // GET api/books5
        public IHttpActionResult Get(int id)//IHttpActionResult becuse we need to send specific book
        {
            var book = books.GetBook(id);
            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        /***********************************ADD New Book*********************************************************/
        //model binding
        [HttpPost]//this not regular method and its a web service
        public IHttpActionResult PostBook(Book book)
        {
           bool result=  books.AddNewBook(book);
           if (result)
           {
                //return Ok(books);//you can see all the books
                return Ok(book);
            }

           return BadRequest();

        }



       /*************************************DeleteBook***************************************************/
        [HttpDelete]//identify this method as web service
        public IHttpActionResult DeleteBook(int id)
        {
            if (books.Remove(id))
            {
                return Ok(books.GetAllBooks());
            }

            return NotFound();
        }


        /***************************************UpdateBook***********************************************/
        [HttpPut]
        public IHttpActionResult UpdateBook(int id, Book book)
        {
            var ubook = books.UpdateBook(id, book);
            if (ubook != null)
            {
                return Ok(ubook);
            }

            return NotFound();
        }

    }
}

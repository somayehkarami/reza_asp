using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ViewInDetails.Infrastructure;

namespace ViewInDetails
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //forget about prievious 
            ViewEngines.Engines.Clear();
            //go to this class and find view so i customize the location by using this method
            ViewEngines.Engines.Add(new CustomLocationViewEngine());
        }
    }
}

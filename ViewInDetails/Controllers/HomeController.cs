﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewInDetails.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            string[] fruits = {"Apple", "Orange", "Pear"};
            return View(fruits);
        }

        public ActionResult MyAction()
        {
            string[] fruits = { "Apple", "Orange", "Pear" };
            return View(fruits);
        }
         
        [ChildActionOnly]
        public ActionResult Time()
        {
            return PartialView(DateTime.Now);
        }
    }
}
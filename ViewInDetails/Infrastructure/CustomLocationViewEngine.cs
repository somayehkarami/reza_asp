﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;

namespace ViewInDetails.Infrastructure
{
    public class CustomLocationViewEngine:RazorViewEngine
    {
        public CustomLocationViewEngine()
        {
            //{1}=> for controller   
            //{0}=> for view
             ViewLocationFormats = new string[] {"~/Views/{1}/{0}.cshtml", "~/Views/{Common}/{0}.cshtml" };
        }
        //we should declare  in Global.asax that we created subclass of Razor view

    }
}
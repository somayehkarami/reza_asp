﻿using System.Collections.Generic;
using System.Web.Mvc;
using BlueSky.Models;
using BlueSky.ViewModels;

namespace BlueSky.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movies/Random
        /*
           Action Result is actually a data type. When it is used with action method, it is called return type.
           Action Result is a base data type whose derived types are HttpStatusCodeResult, JavaScriptResult, FileResult, ContentResult, JsonResult, EmptyResult, RedirectResult, RedirectToRouteResult, ViewResult. And, there must be a helper method against each data type (either base or derived type).*/


        public ActionResult Random() //actionResult "baseclass" for all actionResult in ASP.NET 
        {
            var movie = new Movie() {name = "Shrek!"};
            var customers = new List<Customer>
            {
                new Customer() {Name = "Customer 1"},
                new Customer() {Name = "Customer 1"}
            };
            var viewModel = new RandomMovieViewModel()
            {
                Movie = movie,
                Customers = customers
            };

            return View(viewModel);//inherent from controllers
                               // Movie movie = new Movie();
                               //movie.name = "Sherek!";
                               // return Content("Hello World!");
                               //return HttpNotFound();
                               //return  new EmptyResult();
                               // return RedirectToAction("Index", "Home", new {page = 1, sortBy = "name"});

        }

        /********************************************using parameter sources************************************/
        public ActionResult Edit(int id)
        {
            return Content("id= " + id);
        }

        //make nullable with "?" and string type by defalut is nullable
        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
                pageIndex = 1;

            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "Name";
            }

            return Content(string.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));

        }

        [Route("movies/released/{year}/{month:regex(\\d{2}):range(1,12)}")]

        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);
        }



    }
}
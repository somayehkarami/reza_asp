﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BlueSky
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            /************************Ruthing by using attribute*****************************************/
            routes.MapMvcAttributeRoutes();





            /* //add before MapRoute because of order
             routes.MapRoute(
                 name: "MoviesByReleaseDate",
                 url: "movies/released/{year}/{month}",
                 defaults: new { controller = "Movies", action = "ByReleaseDate" },
                 new {year = @"\d{4}",month=@"\d{2}"});//condition
             //new {year = @" 2019|2020",month=@"\d{2}"});//condition*/
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
//Movie/release/2015/04
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustoHelperMethods.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Fruits = new string[] {"Apple", "Orang", "Pear"};
            ViewBag.Cities = new string[] {"Paris","Madrid","Rome","Montreal" };
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustoHelperMethods.Infrastructure
{
    //dont forget to create static class!!!!!!!!!!!!!!!

    public  static class CustomHelperMethod
    {
        //"this" means I want to extend this method
        public static MvcHtmlString ListArrayItems(this HtmlHelper html, string[] list)
        {
            TagBuilder tag = new TagBuilder("ul");

            foreach (string str in list)
            {
                TagBuilder itemTag = new TagBuilder("li");
                itemTag.SetInnerText(str);
                tag.InnerHtml += itemTag.ToString();
               // tag.AddCssClass();
            }

            return new MvcHtmlString(tag.ToString());
        }
    }
}